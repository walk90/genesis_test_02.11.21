variable access_key {
  default = ""
}

variable secret_key {
  default = ""
}

variable region {
  default = "eu-central-1"
}

variable ami {
  default = "ami-0245697ee3e07e755"
}

variable instance_type {
  default = "t2.small"
}

variable public_key {
  default = ""
}

