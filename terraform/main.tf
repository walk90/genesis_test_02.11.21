provider "aws" {
	access_key = "${var.access_key}"
        secret_key = "${var.secret_key}"
        region = "${var.region}"
}

resource "aws_instance" "first_instance" {
   ami = "${var.ami}"
   instance_type = "${var.instance_type}"
   vpc_security_group_ids = [aws_security_group.first_instance.id]
   key_name = "terraform_ec2_key"
}

resource "aws_security_group" "first_instance" {
  name        = "WebServer"
  description = "Security group for Webserver"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "terraform_ec2_key" {
  key_name = "terraform_ec2_key"
  public_key = "${var.public_key}"
}
